terraform {
  required_providers {
    google = {
      source = "hashicorp/google"
      version = "4.27.0"
    }
  }
}

terraform {
  backend "gcs" {
    bucket  = "bucket1-store-tfile" #BUCKET IS ALREADY CREATED IN GCP
    prefix  = "terraform/TFstateFILE"
    credentials = "keyfile.json"
  }
}
